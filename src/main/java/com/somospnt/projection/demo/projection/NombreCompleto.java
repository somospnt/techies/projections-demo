package com.somospnt.projection.demo.projection;

import org.springframework.beans.factory.annotation.Value;

public interface NombreCompleto {

    String getNombre();

    String getApellido();

    @Value("#{target.nombre + ' ' + target.apellido}")
    String getNombreCompleto();

    @Value("#{@usuarioUtil.getNombreCompleto(target)}")
    String getNombreCompletoBean();

    default String getNombreCompletoDefault() {
        return getNombre().concat(" ").concat(getApellido());
    }

}
