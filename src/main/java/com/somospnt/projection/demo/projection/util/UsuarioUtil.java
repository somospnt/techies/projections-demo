package com.somospnt.projection.demo.projection.util;

import com.somospnt.projection.demo.domain.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UsuarioUtil {

    public String getNombreCompleto(Usuario usuario) {
        return usuario.getNombre().concat(" ").concat(usuario.getApellido());
    }

}
