package com.somospnt.projection.demo.repository;

import com.somospnt.projection.demo.domain.Usuario;
import com.somospnt.projection.demo.projection.NombreCompleto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    List<NombreCompleto> findByNombre(String nombre);
}
