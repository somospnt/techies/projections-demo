package com.somospnt.projection.demo.repository;

import com.somospnt.projection.demo.projection.NombreCompleto;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioRepositoryTest {
    
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void buscarTodos_retornaProyeccion() {
        List<NombreCompleto> nombreCompletos = usuarioRepository.findByNombre("Tony");
        Assert.assertNotNull(nombreCompletos);
        Assert.assertTrue(nombreCompletos.size() > 0);
        Assert.assertEquals("Tony Montana", nombreCompletos.get(0).getNombreCompleto());
        Assert.assertEquals("Tony Montana", nombreCompletos.get(0).getNombreCompletoDefault());
        Assert.assertEquals("Tony Montana", nombreCompletos.get(0).getNombreCompletoBean());
    }

}
